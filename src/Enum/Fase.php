<?php
namespace App\Enum;

enum Fase :string
{
    case GRUPO   = "GRUPO";
    case PLAYOFF = 'PLAYOFF';

    public function nombre(): string
    {
        return match ($this) {
            Fase::GRUPO => 'Fase de grupos',
            Fase::PLAYOFF => 'Playoff'
        };
    }

    public function permitePenales(): ?bool
    {
        return match ($this) {
            Fase::GRUPO => false,
            default => true
        };
    }

}