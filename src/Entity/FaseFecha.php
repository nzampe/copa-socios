<?php

namespace App\Entity;

use App\Repository\FaseFechaRepository;
use Doctrine\ORM\Mapping as ORM;

use App\Enum\Fase;

#[ORM\Entity(repositoryClass: FaseFechaRepository::class)]
class FaseFecha
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 25, enumType: Fase::class)]
    private ?Fase $fase = null;

    #[ORM\Column(length: 50)]
    private ?string $fecha = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFase(): ?Fase
    {
        return $this->fase;
    }

    public function setFase(Fase $fase): self
    {
        $this->fase = $fase;

        return $this;
    }


    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }
}
