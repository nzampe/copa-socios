<?php

namespace App\Entity;

use App\Repository\PartidoRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PartidoRepository::class)]
class Partido
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Equipo $equipoLocal = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?equipo $equipoVisitante = null;

    #[ORM\Column]
    private ?int $golesEquipoLocal = null;

    #[ORM\Column]
    private ?int $golesEquipoVisitante = null;

    #[ORM\Column(nullable: true)]
    private ?int $penalesLocal = null;

    #[ORM\Column(nullable: true)]
    private ?int $penalesVisitante = null;

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $resultado = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?FaseFecha $fecha = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dia = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?TipoCopa $tipoCopa = null;

    public function __construct($equipoLocal, $equipoVisitante, $fecha, $tipoCopa)
    {
        $this->equipoLocal     = $equipoLocal;
        $this->equipoVisitante = $equipoVisitante;
        $this->fecha           = $fecha;
        $this->tipoCopa        = $tipoCopa;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEquipoLocal(): ?Equipo
    {
        return $this->equipoLocal;
    }

    public function setEquipoLocal(?Equipo $equipoLocal): self
    {
        $this->equipoLocal = $equipoLocal;

        return $this;
    }

    public function getEquipoVisitante(): ?equipo
    {
        return $this->equipoVisitante;
    }

    public function setEquipoVisitante(?equipo $equipoVisitante): self
    {
        $this->equipoVisitante = $equipoVisitante;

        return $this;
    }

    public function getGolesEquipoLocal(): ?int
    {
        return $this->golesEquipoLocal;
    }

    public function setGolesEquipoLocal(int $golesEquipoLocal): self
    {
        $this->golesEquipoLocal = $golesEquipoLocal;

        return $this;
    }

    public function getGolesEquipoVisitante(): ?int
    {
        return $this->golesEquipoVisitante;
    }

    public function setGolesEquipoVisitante(int $golesEquipoVisitante): self
    {
        $this->golesEquipoVisitante = $golesEquipoVisitante;

        return $this;
    }

    public function getPenalesLocal(): ?int
    {
        return $this->penalesLocal;
    }

    public function setPenalesLocal(?int $penalesLocal): self
    {
        $this->penalesLocal = $penalesLocal;

        return $this;
    }

    public function getPenalesVisitante(): ?int
    {
        return $this->penalesVisitante;
    }

    public function setPenalesVisitante(?int $penalesVisitante): self
    {
        $this->penalesVisitante = $penalesVisitante;

        return $this;
    }

    public function getResultado(): ?string
    {
        return $this->resultado;
    }

    public function setResultado(?string $resultado): self
    {
        $this->resultado = $resultado;

        return $this;
    }

    public function getFecha(): ?FaseFecha
    {
        return $this->fecha;
    }

    public function setFecha(?FaseFecha $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getDia(): ?\DateTimeInterface
    {
        return $this->dia;
    }

    public function setDia(?\DateTimeInterface $dia): self
    {
        $this->dia = $dia;

        return $this;
    }

    public function getTipoCopa(): ?TipoCopa
    {
        return $this->tipoCopa;
    }

    public function setTipoCopa(?TipoCopa $tipoCopa): self
    {
        $this->tipoCopa = $tipoCopa;

        return $this;
    }
}
