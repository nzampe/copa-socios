<?php

namespace App\Entity;

use App\Repository\EstadisticaJugadorPartidoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EstadisticaJugadorPartidoRepository::class)]
class EstadisticaJugadorPartido
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $goles = null;

    #[ORM\Column]
    private ?int $amarillas = null;

    #[ORM\Column]
    private ?int $rojas = null;

    #[ORM\ManyToOne]
    private ?Jugador $jugador = null;

    #[ORM\ManyToOne]
    private ?Partido $partido = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGoles(): ?int
    {
        return $this->goles;
    }

    public function setGoles(int $goles): self
    {
        $this->goles = $goles;

        return $this;
    }

    public function getAmarillas(): ?int
    {
        return $this->amarillas;
    }

    public function setAmarillas(int $amarillas): self
    {
        $this->amarillas = $amarillas;

        return $this;
    }

    public function getRojas(): ?int
    {
        return $this->rojas;
    }

    public function setRojas(int $rojas): self
    {
        $this->rojas = $rojas;

        return $this;
    }

    public function getJugador(): ?Jugador
    {
        return $this->jugador;
    }

    public function setJugador(?Jugador $jugador): self
    {
        $this->jugador = $jugador;

        return $this;
    }

    public function getPartido(): ?Partido
    {
        return $this->partido;
    }

    public function setPartido(?Partido $partido): self
    {
        $this->partido = $partido;

        return $this;
    }
}
