<?php

namespace App\Entity;

use App\Repository\JugadorRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JugadorRepository::class)]
class Jugador
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 75)]
    private ?string $nombre = null;

    #[ORM\Column(length: 75)]
    private ?string $apellido = null;

    #[ORM\Column(length: 20)]
    private ?string $dni = null;

    #[ORM\Column(length: 12)]
    private ?string $nroSocio = null;

    #[ORM\ManyToOne(inversedBy: 'jugadores')]
    private ?Equipo $equipo = null;

    public function __construct($nombre, $apellido, $dni, $nroSocio, $equipo)
    {
        $this->nombre   = $nombre;
        $this->apellido = $apellido;
        $this->dni      = $dni;
        $this->nroSocio = $nroSocio;
        $this->equipo   = $equipo;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getNroSocio(): ?string
    {
        return $this->nroSocio;
    }

    public function setNroSocio(string $nroSocio): self
    {
        $this->nroSocio = $nroSocio;

        return $this;
    }

    public function getEquipo(): ?Equipo
    {
        return $this->equipo;
    }

    public function setEquipo(?Equipo $equipo): self
    {
        $this->equipo = $equipo;

        return $this;
    }
}
