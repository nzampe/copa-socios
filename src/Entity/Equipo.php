<?php

namespace App\Entity;

use App\Repository\EquipoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EquipoRepository::class)]
class Equipo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 75)]
    private ?string $nombre = null;

    #[ORM\Column]
    private ?int $puntos = null;

    #[ORM\Column]
    private ?int $golesF = null;

    #[ORM\Column]
    private ?int $golesC = null;

    #[ORM\Column]
    private ?int $golesDif = null;

    #[ORM\ManyToOne]
    private ?Zona $zona = null;

    #[ORM\OneToMany(mappedBy: 'equipo', targetEntity: Jugador::class)]
    private Collection $jugadores;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?TipoCopa $tipoCopa = null;

    public function __construct($nombre, $zona)
    {
        $this->nombre    = $nombre;
        $this->zona      = $zona;
        $this->puntos    = 0;
        $this->golesF    = 0;
        $this->golesC    = 0;
        $this->golesDif  = 0;
        $this->jugadores = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getPuntos(): ?int
    {
        return $this->puntos;
    }

    public function setPuntos(int $puntos): self
    {
        $this->puntos = $puntos;

        return $this;
    }

    public function getGolesF(): ?int
    {
        return $this->golesF;
    }

    public function setGolesF(int $golesF): self
    {
        $this->golesF = $golesF;

        return $this;
    }

    public function getGolesC(): ?int
    {
        return $this->golesC;
    }

    public function setGolesC(int $golesC): self
    {
        $this->golesC = $golesC;

        return $this;
    }

    public function getGolesDif(): ?int
    {
        return $this->golesDif;
    }

    public function setGolesDif(int $golesDif): self
    {
        $this->golesDif = $golesDif;

        return $this;
    }

    public function getZona(): ?Zona
    {
        return $this->zona;
    }

    public function setZona(?Zona $zona): self
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * @return Collection<int, Jugador>
     */
    public function getJugadores(): Collection
    {
        return $this->jugadores;
    }

    // public function addJugador(Jugador $jugador): self
    // {
    //     if (!$this->jugadores->contains($jugador)) {
    //         $this->jugadores->add($jugador);
    //         $jugador->setEquipo($this);
    //     }

    //     return $this;
    // }

    // public function removeJugador(Jugador $jugador): self
    // {
    //     if ($this->jugadores->removeElement($jugador)) {
    //         // set the owning side to null (unless already changed)
    //         if ($jugador->getEquipo() === $this) {
    //             $jugador->setEquipo(null);
    //         }
    //     }

    //     return $this;
    // }

    public function getTipoCopa(): ?TipoCopa
    {
        return $this->tipoCopa;
    }

    public function setTipoCopa(?TipoCopa $tipoCopa): self
    {
        $this->tipoCopa = $tipoCopa;

        return $this;
    }
}
