<?php

namespace App\Repository;

use App\Entity\EstadisticaJugadorPartido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EstadisticaJugadorPartido>
 *
 * @method EstadisticaJugadorPartido|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstadisticaJugadorPartido|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstadisticaJugadorPartido[]    findAll()
 * @method EstadisticaJugadorPartido[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstadisticaJugadorPartidoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EstadisticaJugadorPartido::class);
    }

    public function save(EstadisticaJugadorPartido $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(EstadisticaJugadorPartido $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return EstadisticaJugadorPartido[] Returns an array of EstadisticaJugadorPartido objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EstadisticaJugadorPartido
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
