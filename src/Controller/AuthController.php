<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use App\Repository\UserRepository;
use App\Entity\User;

#[Rest\Route('/admin')]
class AuthController extends AbstractFOSRestController
{

    private UserPasswordHasherInterface $passwordHasher;
    private UserRepository $userRepository;

    public function __construct(
        UserPasswordHasherInterface  $passwordHasher,
        UserRepository $userRepository
    ) {
        $this->passwordHasher = $passwordHasher;
        $this->userRepository = $userRepository;
    }

    #[Rest\Post('/login', name: 'admin_login')]
    public function login()
    {
    }

    /* #[Rest\Post('/test', name: 'app_test')]
    #[Rest\RequestParam(name: 'username', description: 'E-mail', strict: true, nullable: false)]
    #[Rest\RequestParam(name: 'password', description: 'Password', strict: true, nullable: false)]
    public function index(ParamFetcherInterface $paramFetcher): Response
    {
        \extract($paramFetcher->all());

        $usuario = new User();
        $usuario->setUsername($username);
        $hashedPassword = $this->passwordHasher->hashPassword(
            $usuario,
            $password
        );
        $usuario->setPassword($hashedPassword);
        $this->userRepository->save($usuario, true);


        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/TestController.php',
        ]);
    } */
}
