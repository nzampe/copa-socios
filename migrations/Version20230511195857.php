<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230511195857 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE equipo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE estadistica_jugador_partido_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE fase_fecha_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE jugador_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE partido_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tipo_copa_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE zona_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE equipo (id INT NOT NULL, zona_id INT DEFAULT NULL, tipo_copa_id INT NOT NULL, nombre VARCHAR(75) NOT NULL, puntos INT NOT NULL, goles_f INT NOT NULL, goles_c INT NOT NULL, goles_dif INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C49C530B104EA8FC ON equipo (zona_id)');
        $this->addSql('CREATE INDEX IDX_C49C530B2E2179A2 ON equipo (tipo_copa_id)');
        $this->addSql('CREATE TABLE estadistica_jugador_partido (id INT NOT NULL, jugador_id INT DEFAULT NULL, partido_id INT DEFAULT NULL, goles INT NOT NULL, amarillas INT NOT NULL, rojas INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_101B4A8DB8A54D43 ON estadistica_jugador_partido (jugador_id)');
        $this->addSql('CREATE INDEX IDX_101B4A8D11856EB4 ON estadistica_jugador_partido (partido_id)');
        $this->addSql('CREATE TABLE fase_fecha (id INT NOT NULL, fase VARCHAR(25) NOT NULL, fecha VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE jugador (id INT NOT NULL, equipo_id INT DEFAULT NULL, nombre VARCHAR(75) NOT NULL, apellido VARCHAR(75) NOT NULL, dni VARCHAR(20) NOT NULL, nro_socio VARCHAR(12) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_527D6F1823BFBED ON jugador (equipo_id)');
        $this->addSql('CREATE TABLE partido (id INT NOT NULL, equipo_local_id INT NOT NULL, equipo_visitante_id INT NOT NULL, fecha_id INT NOT NULL, tipo_copa_id INT NOT NULL, goles_equipo_local INT NOT NULL, goles_equipo_visitante INT NOT NULL, penales_local INT DEFAULT NULL, penales_visitante INT DEFAULT NULL, resultado VARCHAR(30) DEFAULT NULL, dia TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4E79750B88774E73 ON partido (equipo_local_id)');
        $this->addSql('CREATE INDEX IDX_4E79750B8C243011 ON partido (equipo_visitante_id)');
        $this->addSql('CREATE INDEX IDX_4E79750B9790D82B ON partido (fecha_id)');
        $this->addSql('CREATE INDEX IDX_4E79750B2E2179A2 ON partido (tipo_copa_id)');
        $this->addSql('CREATE TABLE tipo_copa (id INT NOT NULL, nombre VARCHAR(40) NOT NULL, formato VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE zona (id INT NOT NULL, nombre VARCHAR(40) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE equipo ADD CONSTRAINT FK_C49C530B104EA8FC FOREIGN KEY (zona_id) REFERENCES zona (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE equipo ADD CONSTRAINT FK_C49C530B2E2179A2 FOREIGN KEY (tipo_copa_id) REFERENCES tipo_copa (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE estadistica_jugador_partido ADD CONSTRAINT FK_101B4A8DB8A54D43 FOREIGN KEY (jugador_id) REFERENCES jugador (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE estadistica_jugador_partido ADD CONSTRAINT FK_101B4A8D11856EB4 FOREIGN KEY (partido_id) REFERENCES partido (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jugador ADD CONSTRAINT FK_527D6F1823BFBED FOREIGN KEY (equipo_id) REFERENCES equipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partido ADD CONSTRAINT FK_4E79750B88774E73 FOREIGN KEY (equipo_local_id) REFERENCES equipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partido ADD CONSTRAINT FK_4E79750B8C243011 FOREIGN KEY (equipo_visitante_id) REFERENCES equipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partido ADD CONSTRAINT FK_4E79750B9790D82B FOREIGN KEY (fecha_id) REFERENCES fase_fecha (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partido ADD CONSTRAINT FK_4E79750B2E2179A2 FOREIGN KEY (tipo_copa_id) REFERENCES tipo_copa (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE equipo_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE estadistica_jugador_partido_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE fase_fecha_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE jugador_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE partido_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tipo_copa_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE zona_id_seq CASCADE');
        $this->addSql('ALTER TABLE equipo DROP CONSTRAINT FK_C49C530B104EA8FC');
        $this->addSql('ALTER TABLE equipo DROP CONSTRAINT FK_C49C530B2E2179A2');
        $this->addSql('ALTER TABLE estadistica_jugador_partido DROP CONSTRAINT FK_101B4A8DB8A54D43');
        $this->addSql('ALTER TABLE estadistica_jugador_partido DROP CONSTRAINT FK_101B4A8D11856EB4');
        $this->addSql('ALTER TABLE jugador DROP CONSTRAINT FK_527D6F1823BFBED');
        $this->addSql('ALTER TABLE partido DROP CONSTRAINT FK_4E79750B88774E73');
        $this->addSql('ALTER TABLE partido DROP CONSTRAINT FK_4E79750B8C243011');
        $this->addSql('ALTER TABLE partido DROP CONSTRAINT FK_4E79750B9790D82B');
        $this->addSql('ALTER TABLE partido DROP CONSTRAINT FK_4E79750B2E2179A2');
        $this->addSql('DROP TABLE equipo');
        $this->addSql('DROP TABLE estadistica_jugador_partido');
        $this->addSql('DROP TABLE fase_fecha');
        $this->addSql('DROP TABLE jugador');
        $this->addSql('DROP TABLE partido');
        $this->addSql('DROP TABLE tipo_copa');
        $this->addSql('DROP TABLE zona');
    }
}
